1.
public class lowerCase{
       public static void main(String[] args)
    {
        String str = "Hcl TechNOLogies";

        // Convert the above string to all lowercase.
        String lowerStr = str.toLowerCase();

        // Display the two strings for comparison.
        System.out.println("Original String: " + str);
        System.out.println("String in lowercase: " + lowerStr);
    }
}

2.
public class Replace{
    public static void main(String[] args) {
       String str = "I have demand draft";
       System.out.println("String = "+str);
       System.out.println("Replacing all occurrence of d with h character...");
       System.out.println("Updated string = "+str.replace("d", "h"));
    }
}

3.
import java.util.Scanner;
public class ascendingOrder
{
    public static void main(String[] args) 
    {
        int n, temp;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter no. of elements you want in array:");
        n = s.nextInt();
        int a[] = new int[n];
        System.out.println("Enter all the elements:");
        for (int i = 0; i < n; i++) 
        {
            a[i] = s.nextInt();
        }
        for (int i = 0; i < n; i++) 
        {
            for (int j = i + 1; j < n; j++) 
            {
                if (a[i] > a[j]) 
                {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        System.out.print("Ascending Order:");
        for (int i = 0; i < n - 1; i++) 
        {
            System.out.print(a[i] + ",");
        }
        System.out.print(a[n - 1]);
    }
}

4.
import java.util.Scanner;
public class searchElement
{
    public static void main(String[] args) 
    {
        int n, x, flag = 0, i = 0;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter no. of elements you want in array:");
        n = s.nextInt();
        int a[] = new int[n];
        System.out.println("Enter all the elements:");
        for(i = 0; i < n; i++)
        {
            a[i] = s.nextInt();
        }
        System.out.print("Enter the element you want to find:");
        x = s.nextInt();
        for(i = 0; i < n; i++)
        {
            if(a[i] == x)
            {
                flag = 1;
                break;
            }
            else
            {
                flag = 0;
            }
        }
        if(flag == 1)
        {
            System.out.println("Element found at position:"+(i + 1));
        }
        else
        {
            System.out.println("Element not found");
        }
    }
}

5.
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class add {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String S = in.next();
        int start = in.nextInt();
        int end = in.nextInt();
        System.out.println(S.substring(start,end));
    }
}

6.
import java.util.Scanner;

public class Palindrome{

	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		 System.out.println("Enter string");
		String s = sc.next();
		String rev = "";
		for (int i = s.length()-1; i >=0 ; i--) 
			rev=rev+s.charAt(i);
		if(s.equals(rev))
			System.out.println("String is palindrome");
		else 
			System.out.println("String is not palindrome");

	}

}

7.
public class Pangram{
   public static void main(String[] args) {
      String str = "abcdefghijklmnopqrstuvwxyz";
      boolean[] alphaList = new boolean[26];
      int index = 0;
      int flag = 1;
      for (int i = 0; i < str.length(); i++) {
         if ( str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
            index = str.charAt(i) - 'A';
         }else if( str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
            index = str.charAt(i) - 'a';
      }
      alphaList[index] = true;
   }
   for (int i = 0; i <= 25; i++) {
      if (alphaList[i] == false)
      flag = 0;
   }
   System.out.print("String: " + str);
   if (flag == 1)
      System.out.print("\nThe above string is a pangram.");
   else
      System.out.print("\nThe above string is not a pangram.");
   }
}


8.
public class UserMainCode {
public static String getString(String old){
if(old.charAt(0)=='k' || old.charAt(0)=='K') {
String mod=old.charAt(0)+old.substring(2);
return mod;
}
else if (old.charAt(1)=='b' || old.charAt(1)=='B')
String mod=old.charAt(1)+old.substring(2);
return mod;
}
else{
String mod=old.substring(2);
return mod;
}
}
}
public class Main{
public static void main(String[] args)
{
Scanner sc=new Scanner(System.in);
System.out.println("Enter the string");
String str=sc.nextLine();
System.out.println(UserMaincode.getString(str));
sc.close();
}
}

9.

import java.util.*;
public class reverse {
public static void main(String[] args) {
Scanner sc = new Scanner(System.in);
String s1 = sc.nextLine();
reShape(s1);
}
public static String reShape(String s) {
 
StringBuffer sb = new StringBuffer(s);
StringBuffer sb2 = new StringBuffer();
String s2 = sb.reverse().toString();
for (int i = 0; i < s2.length(); i++) {
sb2.append(s2.charAt(i));
sb2.append("-");
}
sb2.deleteCharAt(sb2.length() - 1);
System.out.println(sb2.toString());
return sb2.toString();
}
}

10.
package com.hcl;
public class Calculator {
public int add(int a, int b){
return a+b;
}
}

import demo.com.hcl;
public class Demo{
   public static void main(String args[]){
	Calculator obj = new Calculator();
	System.out.println(obj.add(100, 200));
   }
}